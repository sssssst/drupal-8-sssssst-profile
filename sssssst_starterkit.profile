<?php

/**
 * @file
 * Enables modules and site configuration for a sssssst starterkit site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function sssssst_starterkit_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  
  // Website information
  // $domain = str_ireplace('www.', '', parse_url($_SERVER['SERVER_NAME'], PHP_URL_HOST));
  // $form['site_information']['site_name']['#default_value'] = '';	  
  $form['site_information']['site_mail']['#default_value'] = 'info@' . str_ireplace('www.', '', $_SERVER['SERVER_NAME']);	  

  // Account information defaults
  $form['admin_account']['account']['name']['#default_value'] = 'sssssst';
  $form['admin_account']['account']['mail']['#default_value'] = 'po@sssss.st';

  // Date/time settings
  $form['regional_settings']['site_default_country']['#default_value'] = 'NL';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Amsterdam';
  
  // Notifications
  $form['update_notifications']['enable_update_status_module']['#default_value'] = 0;
  
  $form['#submit'][] = 'sssssst_starterkit_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function sssssst_starterkit_form_install_configure_submit($form, FormStateInterface $form_state) {
  // $site_mail = $form_state->getValue('site_mail');
  // ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}
